# 3 Link Robotic Arm
---

## Terminology
---
Q space - space containing orientations of an arm.  
W space - space containing (x, y, z) co-ordinates of arm.

## Abstract
---
Given a 3 dimensional co-ordinate (x, y, z) of particular robot determine orientations of the arm. Solving this problem using inverse kinematics equations requires high computation and power due to complexness of the equation. At first we utilized various unsupervised machine learning
algorithms to solve the problem, but results were worse due to randomness in the data, as we were trying to find the co-relation in unsupervised environment. So we clustered the forward kinematics equation using simple DFS and using this relation between W space and Q space we were 
able to clustered different Q space solutions of W space, thereby allowing us to return W space by picking any Q space solution from the clusters.
