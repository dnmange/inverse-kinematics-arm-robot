%%
%Input theta1 and theta2%
o1 = -pi/2 : pi/180 : 3*pi/4;
o2 = -pi/2 : pi/180 : pi/2;

[o1,o2] = meshgrid( o1,o2);

x = zeros(size(o1,2),size(o2,2));
y = x;

%%
%Generating values of x and y via forward kinematics%
x = 2 * cos(o1) + 1 * cos(o1+o2);
y = 2 * sin(o1) + 1 * sin(o1+o2);
x = x(:);
y = y(:);

%%
%%Generating values of theta1 and theta2 for testing accuracy of models
c2 = 1:length(x);
s2p = 1:length(x); 
s2n = 1:length(x);
solutionInvKin1 = [0 0];
solutionInvKin2 = [0 0];

for i = 1 : length(x)
    c2(i) = (x(i)*x(i) + y(i)*y(i) - 4 - 1)/(2*2*1);
    if c2(i)<=1
        s2p(i) = sqrt(1 -( c2(i)*c2(i)));
        s2n(i) = -sqrt(1 - (c2(i)*c2(i)));
        theta2solution1 = atan2(s2p(i),c2(i));
        theta2solution2 = atan2(s2p(i),c2(i));
        %k1 and k2 for solution1%
        k1 = 2 + cos(theta2solution1);
        k2 = sin(theta2solution1); 
        theta1solution1 =  atan2(y(i),x(i))-atan2(k2,k1);
        %k1 and k2 for solution2%
        k1 = 2 + cos(theta2solution2);
        k2 = sin(theta2solution2);
        theta1solution2 = atan2(y(i),x(i))-atan2(k2,k1);
        solutionInvKin1 = [ solutionInvKin1 ;theta1solution1 theta2solution1];
        solutionInvKin2 = [ solutionInvKin2 ;theta1solution2 theta2solution2];
    end
end

wholeDataSet = [solutionInvKin1(2:length(solutionInvKin1)); solutionInvKin2(2:length(solutionInvKin2))];
trainingData = [wholeDataSet(1:5000,:)];
testingData = [wholeDataSet(5000:length(wholeDataSet),:)];

%%
%Generating data set for training the model
o1 = o1(:);
solution1 = [0 0 0 0];
solution2 = [0 0 0 0];
trainingData1 = [0 0 0 0];
trainingData2 = [0 0 0 0];
indexValueIJ = [0];
remainingData1 = [x y o1];

lenx = length(x)

for i = 1:length(x)
    for j = i+1:length(y)
        if((abs(x(i)-x(j))<=0.001 && abs(y(i)-y(j))<=0.001))
            indexValueIJ = [indexValueIJ; i ; j];
            solution1 = [solution1; x(i) y(i) o1(i) o2(i)];
            solution2 = [solution2; x(j) y(j) o1(j) o2(j)];
        end        
    end
end

for i = 2:length(indexValueIJ)
    trainingData1 = [trainingData1; x(indexValueIJ(i)) y(indexValueIJ(i)) o1(indexValueIJ(i)) o2(indexValueIJ(i))];
end

for i = 1:length(x)
    if(ismember(i,indexValueIJ) == false)
        trainingData2 = [trainingData2; x(i) y(i) o1(i) o2(i)];
    end
end

trainingData1x = [trainingData1(2:length(trainingData1),1)];
trainingData1y = [trainingData1(2:length(trainingData1),2)];

%dataset = [trainingData1(2:length(trainingData1),3:4)];
dataset =  trainingData;
solutionDataSet = [solution1(2:length(solution1),3:4) solution2(2:length(solution2),3:4)];
centroid1 = solution1(1,3:4); 
centroid2 = solution2(1,3:4);

solutionDataSet1 = [solution1(1,3:4)];
solutionDataSet2 = [solution2(1,3:4)];

%%
%Displaying ellipse of GMM

k = 2;
Sigma = {'diagonal','full'};
nSigma = numel(Sigma);
SharedCovariance = {true,false};
SCtext = {'true','false'};
nSC = numel(SharedCovariance);
d = 500;
x1 = linspace(min(dataset(:,1)) - 2,max(dataset(:,1)) + 2,d);
x2 = linspace(min(dataset(:,2)) - 2,max(dataset(:,2)) + 2,d);
[x1grid,x2grid] = meshgrid(x1,x2);
X0 = [x1grid(:) x2grid(:)];
threshold = sqrt(chi2inv(0.99,2));
options = statset('MaxIter',1000); % Increase number of EM iterations

figure;
c = 1;
count = [0 0 ; 0 0];
for i = 1:nSigma;
    for j = 1:nSC;
        gmfit = fitgmdist(dataset,k,'CovarianceType',Sigma{i},...
            'SharedCovariance',SharedCovariance{j},'Options',options);
        [clusterX, nlogl] = cluster(gmfit,dataset);
        cluster1 = [trainingData1x(clusterX == 1) trainingData1y(clusterX == 1) dataset(clusterX == 1,:)];
        cluster2 = [trainingData1x(clusterX == 2) trainingData1y(clusterX == 2) dataset(clusterX == 2,:)];
        for p=1:length(cluster1)
            for q=p+1:length(solution1)
                if(cluster1(p,1)==cluster1(q,1) && cluster1(p,2)==cluster1(q,2))
                    count(i,j) = count(i,j)+1;
                end
            end
        end
        for p=1:length(cluster2)
            for q=p+1:length(cluster2)
                if(cluster2(p,1)==cluster2(q,1) && cluster2(p,2)==cluster2(q,2))
                    count(i,j) = count(i,j)+1;
                end
            end
        end
        mahalDist = mahal(gmfit,X0);
        subplot(2,2,c);
        h1 = gscatter(dataset(:,1),dataset(:,2),clusterX);
        hold on;
            for m = 1:k;
                idx = mahalDist(:,m)<=threshold;
                Color = h1(m).Color*0.75 + -0.5*(h1(m).Color - 1);
                h2 = plot(X0(idx,1),X0(idx,2),'.','Color',Color,'MarkerSize',1);
                uistack(h2,'bottom');
            end
        plot(gmfit.mu(:,1),gmfit.mu(:,2),'kx','LineWidth',2,'MarkerSize',10)
        title(sprintf('Sigma is %s, SharedCovariance = %s',...
            Sigma{i},SCtext{j}),'FontSize',8)
        legend(h1,{'1','2'});
        hold off
        c = c + 1;
        
    end
end

%%
%Display contour of GMM
figure;
scatter(dataset(:,1),dataset(:,2),10,'.')
hold on

options = statset('Display','final');
obj = fitgmdist(dataset,2,'Options',options);
h = ezcontour(@(l,m)pdf(obj,[l m]));
hold off

X = dataset 
idx = cluster(obj,X);
cluster1 = X(idx == 1,:);
cluster2 = X(idx == 2,:);
%h = ezcontour(@(l,m)pdf(obj,[l m]),[-8 6],[-8 6]);

%%
for i=2:length(solutionDataSet)
    distanceFromSolution1Centroid1 = sqrt((solutionDataSet(i,1).^2 - centroid1(1,1).^2) + (solutionDataSet(i,2).^2 - centroid1(1,2).^2));
    distanceFromSolution1Centroid2 = sqrt((solutionDataSet(i,3).^2 - centroid1(1,1).^2) + (solutionDataSet(i,4).^2 - centroid1(1,2).^2));
    
    distanceFromSolution2Centroid1 = sqrt((solutionDataSet(i,1).^2 - centroid2(1,1).^2) + (solutionDataSet(i,2).^2 - centroid2(1,2).^2));
    distanceFromSolution2Centroid2 = sqrt((solutionDataSet(i,3).^2 - centroid2(1,1).^2) + (solutionDataSet(i,4).^2 - centroid2(1,2).^2));
    
    if(distanceFromSolution1Centroid1>=distanceFromSolution2Centroid1)
        solutionDataSet1 = [ solutionDataSet1; solutionDataSet(i,3:4) ];
        centroid1 = [mean(solutionDataSet1(2:length(solutionDataSet1),1)) mean(solutionDataSet1(2:length(solutionDataSet1),2))];
        
        solutionDataSet2 = [ solutionDataSet2; solutionDataSet(i,1:2) ];
        centroid2 = [mean(solutionDataSet2(2:length(solutionDataSet2),1)) mean(solutionDataSet2(2:length(solutionDataSet2),2))];
    else
        solutionDataSet1 = [ solutionDataSet1; solutionDataSet(i,1:2) ];
        centroid1 = [mean(solutionDataSet1(2:length(solutionDataSet1),1)) mean(solutionDataSet1(2:length(solutionDataSet1),2))];
        
        solutionDataSet2 = [ solutionDataSet2; solutionDataSet(i,3:4) ];
        centroid2 = [mean(solutionDataSet2(2:length(solutionDataSet2),1)) mean(solutionDataSet1(2:length(solutionDataSet2),2))];
    end
    
end

%Plotting forward kinematics%
plot(x(:),y(:),'blue.')
scatter3(x(:),y(:),o1(:));
axis equal;
xlabel('X','fontsize',10)
ylabel('Y','fontsize',10)
title('X-Y Plotting for forward kinematics','fontsize',10)

%Creating data sets for deriving values of theta1 and theta2%
X = x(:);
Y = y(:);
O1 = o1(:);
O2 = o2(:);
tb1 = table(X,Y,O1);
tb2 = table(X,Y,O2);

%Training model using gaussian regression SVM%
svmModelForTheta1 = fitrsvm(tb1,O1,'KernelFunction','gaussian','Standardize',true);
svmModelForTheta2 = fitrsvm(tb2,O2,'KernelFunction','gaussian','Standardize',true);

%Predicting values of O1%
predictedTheta1= predict(svmModelForTheta1,table(X,Y,O1));
predictedTheta2= predict(svmModelForTheta1,table(X,Y,O1));

ObsAndPred = table(predictedTheta1,O1,predictedTheta2,O2);
errorTheta1 = sqrt(sum(abs(predictedTheta1.^2-O1.^2)));





