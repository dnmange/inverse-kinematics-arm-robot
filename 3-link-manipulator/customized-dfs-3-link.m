% Forward kinematics plotting

% Uncomment to test it with base values
% theta1 = [-170 30 170]; 
% theta2 = [-225 30 45];
% theta3 = [-250 30 75];

theta1 = -170:20:170;
theta2 = -225:20:45;
theta3 = -250:20:75;

% converting degrees to radians
theta1 = deg2rad(theta1); theta2 = deg2rad(theta2); theta3 = deg2rad(theta3);

% meshgrid of theta1, theta2 and theta3
[theta1,theta2,theta3] = meshgrid( theta1,theta2,theta3);

% Defining the size of x, y and z
x=[ 0 0 0];
y=x;z=x;
a2=431.8; a3=20.32; d3=124.46; d4=431.8;

%Generation my x,y,z of data
x = cos(theta1).*(a2.*cos(theta2)+a3.*cos(theta2+theta3)-d4.*sin(theta2+theta3))-d3.*sin(theta1);
y = sin(theta1).*(a2.*cos(theta2)+a3.*cos(theta2+theta3)-d4.*sin(theta2+theta3))+d3.*cos(theta1);
z = -a3.*sin(theta2+theta3)-a2.*sin(theta2)-d4.*cos(theta2+theta3);

%storing values of theta1, theta2, theta3, x, y, z
% xyztheta = [x(:) y(:) z(:) theta1(:) theta2(:) theta3(:)];
% xyz = [x(:) y(:) z(:)];
x = x(:);
y = y(:);
z = z(:);

plot3(x,y,z,'.r');
fthetas = [theta1(:) theta2(:) theta3(:)];


%generate theta's values from x,y,z

solution1 = [0 0 0 0 0 0];
solution2 = [0 0 0 0 0 0];
solution3 = [0 0 0 0 0 0];
solution4 = [0 0 0 0 0 0];
for i = 1 : length(x)
    k = (x(i)*x(i) + y(i)*y(i) + z(i)*z(i) - a2*a2 - a3*a3 - d3*d3 - d4*d4)/(2*a2);
    %computing theta1
    theta1p = 2*atan((-x(i)+sqrt(x(i)*x(i) + y(i)*y(i) - d3*d3))/(y(i)+d3));
    theta1n = 2*atan((-x(i)-sqrt(x(i)*x(i) + y(i)*y(i) - d3*d3))/(y(i)+d3));
    
    %computing theta3
    theta3p = 2*atan((-d4+sqrt(a3*a3 + d4*d4 - k*k))/(k+a3));
    theta3n = 2*atan((-d4-sqrt(a3*a3 + d4*d4 - k*k))/(k+a3));
    
    %computing theta2 with theta1p & theta3p
    term1pp = -a2 - a3*cos(theta3p) + d4*sin(theta3p) + sqrt(a2*a2 + a3*a3 + d4*d4 + 2*a2*(a3*cos(theta3p)-d4*sin(theta3p)) - z(i)*z(i));
    term2pp = z(i) - ( a3*sin(theta3p) + d4*cos(theta3p) );
    theta2pp = 2*atan( term1pp/term2pp );
    solution1 = [ solution1; x(i) y(i) z(i) theta1p  theta2pp theta3p];
    
    %computing theta2 with theta1p & theta3n
    term1pn = -a2 - a3*cos(theta3n) + d4*sin(theta3n) + sqrt(a2*a2 + a3*a3 + d4*d4 + 2*a2*(a3*cos(theta3n)-d4*sin(theta3n)) - z(i)*z(i));
    term2pn = z(i) - ( a3*sin(theta3n) + d4*cos(theta3n) ); 
    theta2pn = 2*atan( term1pn/term2pn );
    solution2 = [ solution2; x(i) y(i) z(i) theta1p  theta2pn theta3n];
    
    %computing theta2 with theta1n & theta3p
    term1np = -a2 - a3*cos(theta3p) + d4*sin(theta3p) - sqrt(a2*a2 + a3*a3 + d4*d4 + 2*a2*(a3*cos(theta3p)-d4*sin(theta3p)) - z(i)*z(i));
    term2np = z(i) - ( a3*sin(theta3p) + d4*cos(theta3p) );
    theta2np = 2*atan( term1np/term2np );
    solution3 = [ solution3; x(i) y(i) z(i)  theta1n theta2np theta3p];
    
    %computing theta2 with theta1n & theta3n
    term1nn = -a2 - a3*cos(theta3n) + d4*sin(theta3n) - sqrt(a2*a2 + a3*a3 + d4*d4 + 2*a2*(a3*cos(theta3n)-d4*sin(theta3n)) - z(i)*z(i));
    term2nn = z(i) - ( a3*sin(theta3n) + d4*cos(theta3n) );
    theta2nn = 2*atan( term1nn/term2nn );
    solution4 = [ solution4; x(i) y(i) z(i)  theta1n theta2nn theta3n];
   
end


% dataset from inverse kinematics equation

wholeDataSet = [solution1(2:end,:); solution2(2:end,:); solution3(2:end,:); solution4(2:end,:)];
thetai = wholeDataSet(:,4:6);
theta1 = thetai(:,1);
theta2 = thetai(:,2);
theta3 = thetai(:,3);
fthetas = [theta1 theta2 theta3];

% storing values of w-space from inverse kinematics

x = wholeDataSet(:,1);
y = wholeDataSet(:,2);
z = wholeDataSet(:,3);

stack = java.util.Stack();
dp = zeros(length(x));
minindex = 0;
minvalue = 4000;
%find distance with every other point workspace
for i = 1:length(x)
    for j = i+1:length(x) 
        if calculateWspaceDistance(i,j,x,y,z)==0
            if ~stack.contains(i)
                stack.push(i);
            end
            if ~stack.contains(j)
                stack.push(j);
            end
        end
    end
end


% clustering q-space solutions 

firstElement = 1;
thetas = [0];
clusterX = zeros(length(x));
cluster1 = [0];
cluster2 = [0];
cluster3 = [0];
cluster4 = [0];
visibility = zeros(length(x)); % ensuring to detect cycle.
distanceCover = zeros(length(x)); % once nearest distance are taken , it shouldnt be taken again.

while(~stack.empty())
    idx = stack.pop();
    if visibility(idx) == 0
        visibility(idx) = 1;
        thetas = [thetas; idx];
        distanceCover(idx) = 1;
        for k = 1:length(x)
            if calculateWspaceDistance(idx,k,x,y,z)==0 && k~=idx
                if distanceCover(k) == 0
                    if ~stack.contains(k) 
                        stack.push(k);
                    end
                    
                    distanceCover(k) = 1; 
                    visibility(k) = 1;
                    thetas = [thetas; k];   %store thetas index
                end
            end
        end
        [row,column] = size(thetas);
        if(firstElement)
            clusterX(thetas(2,:)) = 1;          %cluster thetas index
            cluster1 = [cluster1;thetas(2,:)]
            if row>=3
                clusterX(thetas(3,:)) = 2;
                cluster2 = [cluster2;thetas(3,:)]
            end
            if row>=4
                clusterX(thetas(4,:)) = 3;
                cluster3 = [cluster3;thetas(4,:)]
            end
            if row>=5
                clusterX(thetas(5,:)) = 4;
                cluster4 = [cluster4;thetas(5,:)]
            end
        else
            alreadyClustered = java.util.Stack();

            if ~(length(cluster1)==length(cluster2)) || ~(length(cluster2)==length(cluster3)) || ~(length(cluster3)==length(cluster4)) 
                yes = [1];
            end
            for i = 2:row
                dist1 = 0; dist2 = 0; dist3=0;dist4=0;
                
                %check if 4 clusters were filled with 4 closest values if
                %yes, reinitialize stack
                if alreadyClustered.size() == 4
                    alreadyClustered = java.util.Stack();
                end
                %check if one of the closest points was already clustered
                %in the cluster1
                if ~alreadyClustered.contains(1)
                    %calculate distance from cluster1
                    for j = 2:length(cluster1)
                        dist1 = dist1+calcuateThetaDistance(cluster1(j),i,fthetas);

                    end
                    min = [1 dist1];
                else
                    min = [1 -1]; %-1 is to ensure to ignore cluster 1 as one of the values is already in it
                end
                
                %check if one of the closest points was already clustered
                %in the cluster2
                if ~alreadyClustered.contains(2)
                    %calculate distance from cluster2
                    for j = 2:length(cluster2)
                        dist2 = dist2+calcuateThetaDistance(cluster2(j),i,fthetas);
                    end
                else
                    dist2 = -1; %-1 is to ensure to ignore cluster 2 as one of the values is already in it
                end
                
                if(min(1,2)==-1)
                    min = [2 dist2]
                else
                    if(~(dist2==-1) && min(1,2)>dist2)
                        min = [2 dist2];
                    end
                end
                    
                %check if one of the closest points was already clustered
                %in the cluster3
                if ~alreadyClustered.contains(3)
                    %calculate distance from cluster3
                    for j = 2:length(cluster3)
                        dist3 = dist3+calcuateThetaDistance(cluster3(j),i,fthetas);
                    end
                else
                    dist3 = -1; %-1 is to ensure to ignore cluster 3 as one of the values is already in it
                end
                
                if(min(1,2) == -1) 
                    min = [3 dist3];
                else
                    if(~(dist3==-1) && min(1,2)>dist3)
                        min = [3 dist3];
                    end
                end
                
                %check if one of the closest points was already clustered
                %in the cluster3
                if ~alreadyClustered.contains(4)
                    %calculate distance from cluster4
                    for j = 2:length(cluster4)
                        dist4 = dist4+calcuateThetaDistance(cluster4(j),i,fthetas);
                    end
                else
                    dist4 = -1; %-1 is to ensure to ignore cluster 4 as one of the values is already in it
                end
                
                if(min(1,2)==-1)
                    min = [4 dist4]
                else
                    if(~(dist4==-1) && min(1,2)>dist4 )
                        min = [4 dist4];
                    end
                end
                
                clusterX(thetas(i,:)) = min(1,1);
                
                %pick cluster which is closest and append values of theta
                if min(1,1) == 1
                    cluster1 = [cluster1;thetas(i,:)];
                elseif min(1,1) == 2 
                    cluster2 = [cluster2;thetas(i,:)];
                elseif min(1,1) == 3
                    cluster3 = [cluster3;thetas(i,:)];
                else
                    cluster4 = [cluster4;thetas(i,:)];
                end
                alreadyClustered.push(min(1,1));
            end
        end
        firstElement = 0;
        thetas = [0];
    end
end

xyz = [x y z];
xyzcluster1 = xyz(clusterX==1,:);
xyzcluster2 = xyz(clusterX==2,:);
xyzcluster3 = xyz(clusterX==3,:);
xyzcluster4 = xyz(clusterX==4,:);

%computing common cluster for x,y,z from 4 solutions i.e clusters
commonCluster1 = [0 0 0];
for p = 1:length(xyzcluster1)
    if(ismember(xyzcluster1(p,:),xyzcluster2))
        commonCluster1 = [commonCluster1; xyzcluster1(p,:)]
    end
end

commonCluster2 = [0 0 0]
for  p = 1:length(commonCluster1)
    if (ismember(commonCluster1(p,:),xyzcluster3))
        commonCluster2 = [commonCluster2; commonCluster1(p,:)];
    end
end

commonCluster3 = [0 0 0]
for  p = 1:length(commonCluster2)
    if (ismember(commonCluster2(p,:),xyzcluster4))
        commonCluster3 = [commonCluster3; commonCluster2(p,:)];
    end
end

count = 0;
for p = 1:length(commonCluster3)
    for q =  p+1:length(commonCluster3)
        if commonCluster3(p,:)==commonCluster3(q,:)
            count = count+1;
        end
    end
end

function wDist = calculateWspaceDistance(i,j,x,y,z)
    divide = x(j)*x(j)+y(j)*y(j)+z(j)*z(j);
    wDist = (x(j)-x(i))*(x(j)-x(i))+(y(j)-y(i))*(y(j)-y(i))+(z(j)-z(i))*(z(j)-z(i))/divide;
end

function tDist = calcuateThetaDistance(i,j,fthetas)
    distance1 = min(abs(fthetas(i,1) - fthetas(j,1)), 2*pi - abs(fthetas(i,1) - fthetas(j,1)));
    distance2 = min(abs(fthetas(i,2) - fthetas(j,2)), 2*pi - abs(fthetas(i,2) - fthetas(j,2)));
    distance3 = min(abs(fthetas(i,3) - fthetas(j,3)), 2*pi - abs(fthetas(i,3) - fthetas(j,3)));
    tDist =  distance1 + distance2 + distance3;
end